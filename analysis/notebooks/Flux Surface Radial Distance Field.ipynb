{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Decomposing Velocity and Wave Energy Flux in 3D"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Stuart J. Mumford - The University of Sheffield - 2016"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This notebook is a modified version of the Flux Surface Analysis notebook to calculate the radial distance from the orginal field axis for all points on the flux surface to enable identification of kink / sausage modes."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Imports and Setup:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As you may expect this code is quite complex and relies on many external and custom bits of code. Many of the wrapper functions to convert between `numpy` arrays and `tvtk` datasets and to perform various operations on `tvtk` objects have been added to the pysac helper library which you will need."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "%gui wx"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "import sys\n",
    "import os\n",
    "\n",
    "import numpy as np\n",
    "%matplotlib inline\n",
    "import matplotlib.pyplot as plt\n",
    "import yt\n",
    "from mayavi import mlab\n",
    "from tvtk.api import tvtk\n",
    "\n",
    "#pysac imports\n",
    "import pysac.yt\n",
    "import pysac.analysis.tube3D.tvtk_tube_functions as ttf\n",
    "import pysac.plot.mayavi_plotting_functions as mpf\n",
    "from pysac.plot.mayavi_seed_streamlines import SeedStreamline\n",
    "\n",
    "#Import this repos config\n",
    "sys.path.append(\"../..\")\n",
    "from scripts.sacconfig import SACConfig\n",
    "cfg = SACConfig()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "#Define tvtk notebook viewer\n",
    "from IPython.core.display import Image \n",
    "def mlab_view(scene, azimuth = 153, elevation = 62, distance = 400, focalpoint = np.array([  25.,   63.,  60.]), aa=16):\n",
    "    scene.anti_aliasing_frames = aa\n",
    "    mlab.view(azimuth = azimuth, elevation = elevation, distance = distance, focalpoint = focalpoint)\n",
    "    scene.save('offscreen.png', size=(500, 500))\n",
    "    return Image(filename='offscreen.png') "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "cfg.period = 240.0\n",
    "cfg.amp = 'A10'"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Load the data series into `yt`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "timeseries = yt.load(\"/home/stuart/Git/Thesis/thesis/Chapter3/Data/Slog_p240-0_A10_B005_00001.gdf\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "#Define which step\n",
    "n = 300\n",
    "\n",
    "#Slices\n",
    "cube_slice = np.s_[:,:,:-5]\n",
    "x_slice = np.s_[:,:,:,:-5]\n",
    "\n",
    "tube_r = cfg.tube_radii[2]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "#if running this creates a persistant window just get it out of the way!\n",
    "mlab.options.offscreen = True\n",
    "fig = mlab.figure(bgcolor=(1, 1, 1))\n",
    "scene = fig.scene"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "#Use the first timestep\n",
    "ds = timeseries\n",
    "cg = ds.index.grids[0]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The first step is to create a `tvtk` dataset for the magentic field, `mayavi` provides the best and potentially only functional wrapper that allows you to do this for a 3D vector field."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "#Create a bfield tvtk field, in mT\n",
    "bfield = mlab.pipeline.vector_field(cg['mag_field_x'][cube_slice] * 1e3,\n",
    "                                    cg['mag_field_y'][cube_slice] * 1e3, \n",
    "                                    cg['mag_field_z'][cube_slice] * 1e3,\n",
    "                                    name=\"Magnetic Field\",figure=None)\n",
    "#Create a scalar field of the magntiude of the vector field\n",
    "bmag = mlab.pipeline.extract_vector_norm(bfield, name=\"Field line Normals\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "#Define the size of the domain\n",
    "xmax, ymax, zmax = np.array(cg['mag_field_x'][cube_slice].shape) - 1\n",
    "domain = {'xmax':xmax, 'ymax':ymax, 'zmax':zmax}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Creating A Flux Tube"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Fieldline seed points"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The initial conditions used in this work are axissymetric, so a flux tube can be constructed by defining a circle around that axis of symmetry. As the magnetic field is weaker at the top the seed points are placed there and traced downwards through the domian."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "#Add axes:\n",
    "axes, outline = mpf.add_axes(np.array(zip(ds.domain_left_edge,ds.domain_right_edge)).flatten()/1e8)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "surf_seeds_poly = ttf.make_circle_seeds(100, int(tube_r[1:]), **domain)\n",
    "seeds = np.array(surf_seeds_poly.points)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Creation of a Flux Surface"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To compute the surface we access the `tvtk` classes for streamline computation and employ the `RuledSurfaceFilter` to create a surface of polygons from the line. The snippet below is implemented in `pysac.analysis.tube3D.tvtk_tube_functions.create_flux_surface(bfield, surf_seeds)`, but is used verbatim below as an example."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "#Make a streamline instance with the bfield\n",
    "surf_field_lines = tvtk.StreamTracer()\n",
    "#bfield is a mayavi data object, we require a tvtk dataset which can be access thus:\n",
    "surf_field_lines.input = bfield.outputs[0]\n",
    "\n",
    "surf_field_lines.source = surf_seeds_poly\n",
    "surf_field_lines.integrator = tvtk.RungeKutta4()\n",
    "surf_field_lines.maximum_propagation = 1000\n",
    "surf_field_lines.integration_direction = 'backward'\n",
    "surf_field_lines.update()\n",
    "\n",
    "#Create surface from 'parallel' lines\n",
    "surface = tvtk.RuledSurfaceFilter()\n",
    "surface.input = surf_field_lines.output\n",
    "surface.close_surface = True\n",
    "surface.pass_lines = True\n",
    "surface.offset = 0\n",
    "surface.distance_factor = 30\n",
    "surface.ruled_mode = 'point_walk'\n",
    "surface.update()\n",
    "\n",
    "#Set the lines to None to remove the input lines from the output\n",
    "surface.output.lines = None"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To visualise this surface we add it to our running `mayavi` pipeline:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can turn on displaying the edges of the polygons that the surface is comprised of and zoom in to highlight the construction of the surface:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "points = np.array(surface.output.points)\n",
    "points -= [63, 63, 0]\n",
    "points"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "distance = np.sqrt((points[:,0]**2 + points[:,1]**2))\n",
    "distance"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To visualise the vectors in `mayavi` we need to add them to the surface `tvtk` object,"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "pd_dis = tvtk.PointData(scalars=distance)\n",
    "pd_dis.scalars.name = \"distance\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "poly_out = surface.output\n",
    "poly_out.point_data.add_array(pd_dis.scalars)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "flux_surface2 = mlab.pipeline.surface(surface.output)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "#Set the surface component to be the azimuthal component\n",
    "flux_surface2.parent.parent.point_scalars_name = 'distance'\n",
    "\n",
    "flux_surface2.module_manager.scalar_lut_manager.lut.table = plt.get_cmap('Reds')(range(255))*255\n",
    "lim = np.max([np.nanmax(surface.output.point_data.scalars),\n",
    "                  np.abs(np.nanmin(surface.output.point_data.scalars))])\n",
    "flux_surface2.module_manager.scalar_lut_manager.data_range = np.array([0,lim])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Add a colourbar:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "surf_bar = mpf.add_colourbar(flux_surface2, [0.84, 0.2], [0.11,0.31],title='', label_fstring='%#4.2f',\n",
    "                          number_labels=5, orientation=1,lut_manager='scalar')\n",
    "mpf.add_cbar_label(surf_bar,'Distance from original axis\\n          [grid points]')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Below is a render with the surface coloured according to the azimuthal velocity."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "#mlab.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "mlab_view(fig.scene)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Environment (thesis)",
   "language": "python",
   "name": "thesis"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 2
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython2",
   "version": "2.7.11"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 0
}
