Effects of Period on MHD Wave Generation from a Logarithmic Spiral Photospheric Driver
======================================================================================

This repository contains all the material for an investigation into the MHD wave 
generation from a photospheric logarithmic spiral velocity field in a magnetic flux tube.
The simulations are done using the FORTRAN SAC code, and the analysis is performed using
Python and the scientific Python ecosystem.

For full examples and workflow of the analysis and instructions on how to run this repository,
either clone and look in the `analysis/notebooks` directory or look at this [URL](http://nbviewer.ipython.org/urls/bitbucket.org/smumford/period-paper/raw/master/analysis/notebooks/Index.ipynb?create=1).


Technical Notes
---------------

###Compiler Configurations

SAC can be compiled under most FORTRAN compilers, tested ones and their flags
are listed below:

#### GNU FORTRAN
compiler: gfortran
flags: -ffree-form

#### INTEL FORTRAN
compiler:
flags: -free -mcmodel=medium -O3 [-xAVX]

#### PGI FORTRAN
compiler: pgf90
flags: -Mfreeform -w=all

Notes:
------

* The configure script does not write wnames to vac.par in a form that fortran will read, therefore wnames are not in the .out file correctly and are re created from the config file in the GDF translation step.
