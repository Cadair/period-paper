#!/usr/bin/env python

import os
import sys
import time
import subprocess

import pushover

sys.path.append("./scripts/")
import sacconfig

cfg = sacconfig.SACConfig()

#Set the Python virtualenv
#os.system("yton")

for t in range(3):
    print "Run analysis script {}:".format(cfg.tube_radii[t])
#print subprocess.check_output(["./run.py",
#        "analysis",
#        "--tube-r=%s"%cfg.tube_radii[t],
#        "--mpi", "--np=16"])
    os.system("./run.py analysis --tube-r={} --mpi --np=16".format(cfg.tube_radii[t]))

print "Job Complete"
client = pushover.PushoverClient()
client.send_message("Job '%s' complete"%(cfg.str_period))
