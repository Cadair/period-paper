#!/bin/bash
#$ -l h_rt=48:00:00
#$ -cwd -V
#$ -N all_analysis
#$ -j y
#$ -l np=16
#$ -l placement=scatter
#$ -t 2-20

source $HOME/.bashrc

#################################################################
################ Set the Parameters for the array ###############
#################################################################
periods=( 30.0 60.0 90.0 120.0 150.0 180.0 210.0 240.0 270.0 300.0 330.0 360.0
390.0 420.0 450.0 480.0 510.0 540.0 570.0 600.0)
amps=( A20r2 A20 A20r2-3 A10r2 A4r10 A20-r3 A20r2-7 A10 A20-3r2 A4r5 A20r2-11
A10r2-3 A20r2-13 A20-r7 A4r10-3 A5r2 A20r2-17 A20-3 A20r2-19 A2r10 )
fortamps=( "20.d0 * SQRT(2.d0)" "20.d0" "20.d0 * SQRT(2.d0 / 3.d0)"
"10.d0 * SQRT(2.d0)" "4.d0 * SQRT(10.d0)" "20.d0 / SQRT(3.d0)" "20.d0 * SQRT(2.d0 / 7.d0)"
"10.d0" "20.d0 / 3.d0 * SQRT(2.d0)" "4.d0 * SQRT(5.d0)" "20.d0 * SQRT(2.d0 /
11.d0)" "10.d0 * SQRT(2.d0 / 3.d0)" "20.d0 * SQRT(2.d0 / 13.d0)" "20.d0 /
SQRT(7.d0)" "4.d0 * SQRT(10.d0 / 3.d0)" "5.d0 * SQRT(2.d0)" "20.d0 * SQRT(2.d0
/ 17.d0)" "20.d0 / 3.d0" "20.d0 * SQRT(2.d0 / 19.d0)" "2.d0 * SQRT(10.d0)" )

#################################################################
####################### Run the Script ##########################
#################################################################

#### Setup and Configure ####
i=$((SGE_TASK_ID - 1))

BASE_DIR=$HOME/BitBucket/period-paper/
TMP_DIR=$(mktemp -d --tmpdir=/nobackup/shesm/temp_run/)

echo $TMP_DIR

cp -r $BASE_DIR $TMP_DIR
cd $TMP_DIR/period-paper/
run_time=$(echo "${periods[i]} * 10" | bc)

echo ${periods[i]} ${amps[i]} "${fortamps[i]}" ${run_time}
./configure.py set SAC --runtime=${run_time}
./configure.py set driver --period=${periods[i]} --amp=${amps[i]} --fort_amp="${fortamps[i]}";
./configure.py print;
./configure.py compile sac --clean;

#### Run the CODE! ####
echo "SAC will run on the following nodes"
cat $PE_HOSTFILE

echo "Run SAC:"
time python run.py SAC --mpi

echo "Run GDF Translator:"
time python run.py gdf --mpi

radii=("r10" "r30" "r60")

for j in {0..2}
do
    echo ${radii[j]}
    python run.py analysis --tube-r=${radii[j]} --mpi
done

###### I Done it now ########
rm -r $TMP_DIR
pushover -m "Job "${periods[i]}" Complete"
echo "Job "${periods[i]}" Complete"
