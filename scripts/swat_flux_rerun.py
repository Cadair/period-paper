#!/usr/bin/env python

import os
import sys
import time
import subprocess

import pushover

sys.path.append("./scripts/")
import sacconfig

cfg = sacconfig.SACConfig()

#Set the Python virtualenv
os.system("yton")

#os.system("./run.py fluxcalc --mpi --np=16")

for t in range(3):
    print "Run analysis script {}:".format(cfg.tube_radii[t])
    os.system("./run.py surfflux --tube-r={} --mpi --np=16".format(cfg.tube_radii[t]))

print "Job Complete"
client = pushover.PushoverClient()
client.send_message("Job '%s' complete"%(cfg.str_period))
